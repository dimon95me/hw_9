package dimon.java.Coparab;

import java.util.HashMap;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Map<String, Float> cafeMap = new HashMap<>();

        cafeMap.put("Soup pure", 24.8f);
        cafeMap.put("Coctail Rom-Cola", 40f);
        cafeMap.put("Miso soup", 54.2f);
        cafeMap.put("Cake", 25.5f);
        cafeMap.put("Lasania", 46.5f);
        cafeMap.put("Brut wine", 30.3f);
        cafeMap.put("Fruits", 60.5f);
        cafeMap.put("Coca-Cola", 12.5f);

        System.out.println("Set of keys: " + cafeMap.keySet());

        Map<String, Integer> orderMap = new HashMap<>();

        orderMap.put("Coca-Cola", 2);
        orderMap.put("Lasania", 2);
        orderMap.put("Cake", 1);
        orderMap.put("Fruits", 3);
        orderMap.put("Miso soup", 2);

        float bill = 0;

        for (Map.Entry<String, Integer> stringIntegerEntry : orderMap.entrySet()) {
//            System.out.println((stringIntegerEntry.getKey()));
            bill += stringIntegerEntry.getValue() * cafeMap.get(stringIntegerEntry.getKey());
            System.out.println(stringIntegerEntry.getKey() + " : " + stringIntegerEntry.getValue() + " | " + cafeMap.get(stringIntegerEntry.getKey()));
        }

        System.out.println(bill);
    }
}
